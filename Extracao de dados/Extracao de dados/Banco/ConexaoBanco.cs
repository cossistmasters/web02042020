﻿using Extracao_de_dados.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extracao_de_dados.Banco
{
    public class ConexaoBanco
    {
        private string conexaoString = "Password=stifler05;Persist Security Info=True;User ID=sa;Initial Catalog=DB_Transparencia_IPERON;Data Source=LAPTOP-3Q347U0G\\SA";
        //private string conexaoString = "Server=172.16.31.130;Database=DB_Transparencia_IPERON; Trusted_Connection=false; User Id=sa;  Password=iperon@2019; Persist Security Info=true;";
        private SqlConnection connection;
        public string conexaoo()
        {
            this.connection = new SqlConnection(this.conexaoString);
            string retorno;
            try
            {
                this.connection.Open();
                retorno = "Banco Conectado!!";
            }
            catch (Exception ex)
            {

                retorno = ex.Message;
            }
            finally
            {
                this.connection.Close();
            }

            return retorno;
        }

        public IList<string> ListarMatriculas()
        {
            conexaoo();
            connection.Open();
            SqlCommand comando = new SqlCommand("select Matricula from RemuneracaoServidor where Lotacao like 'IPERON%'and Ano=2019 and Mes=12", connection);
            comando.CommandType = CommandType.Text;
            SqlDataReader dr = comando.ExecuteReader();
            IList<string> matriculas = new List<string>();
            if (dr != null)
            {
                while (dr.Read())
                {
                    matriculas.Add(dr["Matricula"].ToString().Trim());
                }
                return matriculas;


            }
            else
            {
                return null;
            }


        }
        public RemuneracaoIperon PopularDadosRemunecao(string matricula)
        {
            conexaoo();
            connection.Open();
            SqlCommand comando = new SqlCommand("select*from RemuneracaoServidor where Lotacao like 'IPERON%'and Ano=2019 and Mes=12 and Matricula=@matricula", connection);
            comando.Parameters.AddWithValue("@matricula", matricula);
            comando.CommandType = CommandType.Text;
            SqlDataReader dr = comando.ExecuteReader();
            RemuneracaoIperon remuneracao = new RemuneracaoIperon();
            if (dr != null && dr.Read())
            {
                remuneracao.Matricula = dr["Matricula"].ToString().Trim();
                remuneracao.Nome = dr["Nome"].ToString().Trim();
                remuneracao.Cargo = dr["Cargo"].ToString().Trim();
                remuneracao.CargaHoraria = Convert.ToInt32(dr["CargaHoraria"]);
                remuneracao.CDSFG = dr["CDSFG"].ToString() == "NULL" ? "0" : dr["CDSFG"].ToString().Trim();
                remuneracao.Classificacao_Func = dr["Classificacao_Func"].ToString().Trim();
                remuneracao.Cpf = dr["Cpf"].ToString().Trim();
                remuneracao.SubLotacao = dr["SubLotacao"].ToString().Trim();
                remuneracao.Sequencia = Convert.ToInt32(dr["Sequencia"].ToString().Trim());
                remuneracao.Lotacao = dr["Lotacao"].ToString().Trim();


                connection.Close();
                return remuneracao;

            }
            else
            {
                connection.Close();
                return null;

            }


        }
        public void ListarPessoas(ViagensE_Diarias v)
        {

            foreach (var item in v.ListaCpf)
            {
                conexaoo();
                connection.Open();

                SqlCommand comando = new SqlCommand("select * from RemuneracaoServidor where Ano=2019 and Lotacao like 'iperon%'and Mes=12 and Cpf=@cpf", connection);
                comando.Parameters.AddWithValue("@cpf", item);
                comando.CommandType = CommandType.Text;
                int cont = 0;
                SqlDataReader drr = comando.ExecuteReader();
                if (drr.Read() && drr != null)
                {
                    VerificarOrdemDePagamento(item, Convert.ToDouble(v.ListaValorTotalPorPessoa[cont]));
                    v.Pessoas.Add(drr["Nome"].ToString());
                    v.Cargo.Add(drr["Cargo"].ToString().Trim());
                    cont++;
                }

                connection.Close();
            }


        }
        public RelacaoPessoas BuscarLotacao(string lotacao)
        {
            conexaoo();
            connection.Open();

            SqlCommand comando = new SqlCommand("select*from Lotacao where Descricao=@descricao", connection);
            comando.Parameters.AddWithValue("@descricao", lotacao);
            comando.CommandType = CommandType.Text;

            SqlDataReader drr = comando.ExecuteReader();
            RelacaoPessoas pensionistaIperon = new RelacaoPessoas();
            if (drr.Read() && drr != null)
            {

                pensionistaIperon.LotacaoId = Guid.Parse(drr["LotacaoId"].ToString());

            }
            connection.Close();
            return pensionistaIperon;

        }
        public bool InserirPensionista(RelacaoPessoas pensionista)
        {
            try
            {
                conexaoo();
                connection.Open();
                SqlCommand comando = new SqlCommand("insert into RelacaoPensionista values(@ano,@mes,@matricula,@cpf,@nome,@cargo,null,null,@cargahora,@classificacao,@localtrabalho,null,@lotacaoid)", connection);
                comando.Parameters.AddWithValue("@ano", pensionista.Ano);
                comando.Parameters.AddWithValue("@mes", pensionista.Mes);
                comando.Parameters.AddWithValue("@matricula", pensionista.Matricula);
                comando.Parameters.AddWithValue("@cpf", pensionista.Cpf);
                comando.Parameters.AddWithValue("@nome", pensionista.Nome);
                comando.Parameters.AddWithValue("@cargo", pensionista.Cargo);              
                comando.Parameters.AddWithValue("@cargahora", pensionista.CargaHoraria);
                comando.Parameters.AddWithValue("@classificacao", pensionista.Classificacao);
                comando.Parameters.AddWithValue("@localtrabalho", pensionista.LocalTrabalho);               
                comando.Parameters.AddWithValue("@lotacaoid", pensionista.LotacaoId);
                comando.CommandType = CommandType.Text;
                comando.ExecuteNonQuery();

                connection.Close();
                return true;
            }
            catch (Exception erro)
            {
                var mesn = erro;
                connection.Close();
                return false;
                throw;
            }

        }
        public bool InserirComissionadosEfetivos(RelacaoPessoas pessoas)
        {
            try
            {
                conexaoo();
                connection.Open();
                SqlCommand comando = new SqlCommand("insert into RelacaoServidor values(@matricula,@nome,@cargo,null,@cargahora,@classificacao,@localtrabalho,@vinculo,@ano,@mes,@lotacaoid,@cpf,null)", connection);
                comando.Parameters.AddWithValue("@ano", pessoas.Ano);
                comando.Parameters.AddWithValue("@mes", pessoas.Mes);
                comando.Parameters.AddWithValue("@matricula", pessoas.Matricula);
                comando.Parameters.AddWithValue("@cpf", pessoas.Cpf);
                comando.Parameters.AddWithValue("@nome", pessoas.Nome);
                comando.Parameters.AddWithValue("@vinculo", pessoas.Vinculo);
                comando.Parameters.AddWithValue("@cargo", pessoas.Cargo);
                comando.Parameters.AddWithValue("@cargahora", pessoas.CargaHoraria);
                comando.Parameters.AddWithValue("@classificacao", pessoas.Classificacao);
                comando.Parameters.AddWithValue("@localtrabalho", pessoas.LocalTrabalho);
                comando.Parameters.AddWithValue("@lotacaoid", pessoas.LotacaoId);
                comando.CommandType = CommandType.Text;
                comando.ExecuteNonQuery();

                connection.Close();
                return true;
            }
            catch (Exception erro)
            {
                var mesn = erro;
                connection.Close();
                return false;
                throw;
            }

        }

        public bool InserirRemuneracao(RemuneracaoIperon remuneracao)
        {
            try
            {
                var cds1 = remuneracao.CDSFG == null ? "0" : remuneracao.CDSFG;
                conexaoo();
                connection.Open();
                SqlCommand comando = new SqlCommand("insert into RemuneracaoServidor values(@ano,@mes,@matricula,9,@nome,@cpf,@cargo,@classificacao,@lotacao,@vencimentos,@auxilios," +
                    "@vantagens,@temporarias,@produtividade,@previdencia,@impostos,@diversos,@tributaveis,@finaceiro,@liquido,@cargahoraria,@cds,@sublot)", connection);
                comando.Parameters.AddWithValue("@ano", remuneracao.Ano);
                comando.Parameters.AddWithValue("@mes", remuneracao.Mes);
                comando.Parameters.AddWithValue("@matricula", remuneracao.Matricula);
                comando.Parameters.AddWithValue("@nome", remuneracao.Nome);
                comando.Parameters.AddWithValue("@cpf", remuneracao.Cpf);
                comando.Parameters.AddWithValue("@cargo", remuneracao.Cargo);
                comando.Parameters.AddWithValue("@classificacao", remuneracao.Classificacao_Func);
                comando.Parameters.AddWithValue("@lotacao", remuneracao.Lotacao);
                comando.Parameters.AddWithValue("@vencimentos", remuneracao.Vencimentos);
                comando.Parameters.AddWithValue("@auxilios", remuneracao.Auxilios);
                comando.Parameters.AddWithValue("@vantagens", remuneracao.Vantagens);
                comando.Parameters.AddWithValue("@temporarias", remuneracao.VbaTemporarias);
                comando.Parameters.AddWithValue("@produtividade", remuneracao.VbaProdutividade);
                comando.Parameters.AddWithValue("@previdencia", remuneracao.VbaPrevidencias);
                comando.Parameters.AddWithValue("@impostos", remuneracao.VbaImpostorenda);
                comando.Parameters.AddWithValue("@diversos", remuneracao.VbaDescontosDiversos);
                comando.Parameters.AddWithValue("@tributaveis", remuneracao.RendimentosTributaveis);
                comando.Parameters.AddWithValue("@finaceiro", remuneracao.TotalDescontosFinanceiros);
                comando.Parameters.AddWithValue("@liquido", remuneracao.Liquido);
                comando.Parameters.AddWithValue("@cargahoraria", remuneracao.CargaHoraria);
                comando.Parameters.AddWithValue("@cds", cds1);
                comando.Parameters.AddWithValue("@sublot", remuneracao.SubLotacao);

                comando.CommandType = CommandType.Text;
                comando.ExecuteNonQuery();

                connection.Close();
                return true;
            }
            catch (Exception erro)
            {
                var mesn = erro;
                connection.Close();
                return false;
                throw;
            }

        }


        public void InserirDiariarias(ViagensE_Diarias viagem)
        {

            int cont = 0;
            foreach (var item in viagem.Pessoas)
            {
                conexaoo();
                connection.Open();
                decimal valoruni = Convert.ToDecimal(viagem.ValorUnitaioDiarias[cont].Replace('.', ','));
                float quantidade = float.Parse(viagem.QuantidadeDiarias[cont].Replace('.', ','));
                decimal valorporPessoa = viagem.ListaValorTotalPorPessoa[cont];
                SqlCommand comando = new SqlCommand("insert into AutorizacaoViagem values(NEWID(),@ValorUniario,@TotalDaViagem,@ValorTotalPorPessoa,@ObjetivoDaViagem,@Credor,@Cpf,@NumeroAutorizacao,@NumroSolicitacao,@Cargo,@QuantidadeDiarias,@TipoViagem,@DataIda,@DataVolta)", connection);
                comando.Parameters.AddWithValue("@ValorUniario", valoruni);
                comando.Parameters.AddWithValue("@TotalDaViagem", viagem.ValorTotalDaViagem);
                comando.Parameters.AddWithValue("@ValorTotalPorPessoa", viagem.ListaValorTotalPorPessoa[cont]);
                comando.Parameters.AddWithValue("@ObjetivoDaViagem", viagem.ObjetivoDaViagem);
                comando.Parameters.AddWithValue("@Credor", item);
                comando.Parameters.AddWithValue("@Cpf", viagem.ListaCpf[cont]);
                comando.Parameters.AddWithValue("@NumeroAutorizacao", viagem.NumeroAutorizacao);
                comando.Parameters.AddWithValue("@NumroSolicitacao", viagem.NumroSolicitacao);
                comando.Parameters.AddWithValue("@Cargo", viagem.Cargo[cont]);
                comando.Parameters.AddWithValue("@QuantidadeDiarias", quantidade);
                comando.Parameters.AddWithValue("@TipoViagem", viagem.TipoViagem);
                comando.Parameters.AddWithValue("@DataIda", viagem.DataIda);
                comando.Parameters.AddWithValue("@DataVolta", viagem.DataVolta);
                comando.CommandType = CommandType.Text;
                comando.ExecuteNonQuery();
                cont++;
                connection.Close();
            }


        }
        public string VerificarOrdemDePagamento(string cpf, double valor)
        {
            conexaoo();
            connection.Open();
            SqlCommand comando = new SqlCommand("SELECT*FROM DespesasPortal where DocCredor=" + cpf + " and valorDespesa=" + valor + " and Exercicio=" + DateTime.Now.Year, connection);
            comando.CommandType = CommandType.Text;
            SqlDataReader dr = comando.ExecuteReader();
            string id = "";
            if (dr != null)
            {
                while (dr.Read())
                {
                    var cpf2 = dr["DocCredor"].ToString();
                    double valor2 = Convert.ToDouble(dr["valorDespesa"]);
                    var documento = dr["Documento"];
                    string data = dr["DataDocumento"].ToString();
                    id = dr["RegistroDespesa"].ToString();
                    AtualizarDespesaDiarias(cpf2, documento.ToString(), Convert.ToDateTime(data), valor2, id);
                }

            }
            return id;
        }


        public void AtualizarDespesaDiarias(string cpf, string documento, DateTime ano, double valor, string id)
        {
            string d = ano.Date.ToString().Substring(0, 10);
            conexaoo();
            connection.Open();
            SqlCommand comando = new SqlCommand("update DespesasPortal set NomOrgao='INSTITUTO DE PREVID. DOS SERVIDORES PUBLICOS', NomDespesa='DESPESA DE DIARIAS - PESSOAL CIVIL',NomFuncao='PREVIDENCIA SOCIAL', Fonte=240 ,DesFonteRecurso='RECURSOS DIRETAMENTE ARRECADADOS',EspecificacaoDespesa=33901400 where DocCredor=@cpf and DataDocumento=@ano and Documento=@documento and valorDespesa=@valor and RegistroDespesa=@id ", connection);
            comando.Parameters.AddWithValue("@id", id);
            comando.Parameters.AddWithValue("@ano", ano);
            comando.Parameters.AddWithValue("@cpf", cpf);
            comando.Parameters.AddWithValue("@documento", documento);
            comando.Parameters.AddWithValue("@valor", valor);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();

        }

    }
}
