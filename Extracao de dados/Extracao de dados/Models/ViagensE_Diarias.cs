﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extracao_de_dados.Models
{
   public class ViagensE_Diarias
    {
        public ViagensE_Diarias()
        {
            DespesasDiariasID = new Guid();
        }

        public Guid DespesasDiariasID { get; set; }


        public IList<string> ValorUnitaioDiarias { get; set; } = new List<string>();       
        public decimal ValorTotalDaViagem { get; set; }
        public IList<decimal> ListaValorTotalPorPessoa { get; set; } = new List<decimal>();
        public string ObjetivoDaViagem { get; set; }
        public IList<string> Pessoas { get; set; }=new List<string>();
        public IList<string> ListaCpf { get; set; } = new List<string>();
        public string NumeroAutorizacao { get; set; }
        public string NumroSolicitacao { get; set; }
        public IList<string> Cargo { get; set; } = new List<string>();
        public IList<string> QuantidadeDiarias { get; set; } = new List<string>();
        public string TipoViagem { get; set; }
        public string DataIda { get; set; }
        public string DataVolta { get; set; }
    }
}
