﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extracao_de_dados.Models
{
   public class RelacaoPessoas
    {
        public int Ano { get; set; }


        public int Mes { get; set; }


        public int Matricula { get; set; }


        public long? Cpf { get; set; }


        public string Nome { get; set; }


        public string Cargo { get; set; }

        public DateTime? DataNascimento { get; set; }


        public DateTime? Admissao { get; set; }


        public int? CargaHoraria { get; set; }


        public string Classificacao { get; set; }


        public string LocalTrabalho { get; set; }


        public string Vinculo { get; set; }

        public Guid LotacaoId { get; set; }

        
    }
}

