﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extracao_de_dados.Models
{
  public  class RemuneracaoIperon
    {
        
        public short Ano { get; set; }
        public byte Mes { get; set; }       
        public string Matricula { get; set; }        
        public int? Sequencia { get; set; }        
        public string Nome { get; set; }        
        public string Cpf { get; set; }       
        public string Cargo { get; set; }       
        public string Classificacao_Func { get; set; }
        public string Lotacao { get; set; }      
        public string CDSFG { get; set; }       
        public string SubLotacao { get; set; }     
        public decimal? Vencimentos { get; set; }        
        public decimal? Auxilios { get; set; }       
        public decimal? Vantagens { get; set; }
        public decimal? VbaTemporarias { get; set; }
        public decimal? VbaProdutividade { get; set; }      
        public decimal? VbaPrevidencias { get; set; }       
        public decimal? VbaImpostorenda { get; set; }       
        public decimal? VbaDescontosDiversos { get; set; }        
        public decimal? RendimentosTributaveis { get; set; }       
        public decimal? TotalDescontosFinanceiros { get; set; }        
        public decimal? Liquido { get; set; }
        public int? CargaHoraria { get; set; }
    }
}
