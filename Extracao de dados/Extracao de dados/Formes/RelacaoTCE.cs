﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Extracao_de_dados.Formes
{
    public partial class RelacaoTCE : Form
    {
        public RelacaoTCE()
        {
            InitializeComponent();
        }

        private void NovaGuia(string url)
        {
            TabPage tab = new TabPage();
            tab.Text = "Nova Guia";

            tabControl1.Controls.Add(tab);
            tabControl1.SelectTab(tabControl1.TabCount - 1);
            WebBrowser browser = new WebBrowser() { ScriptErrorsSuppressed = true };
            browser.Parent = tab;
            browser.Dock = DockStyle.Fill;
            richTextBox1.AppendText("Extraindo Dados..." + '\n');
            browser.Navigate(url);
            txtUrl.Text = url;

            browser.DocumentCompleted += webBrowser1_DocumentCompleted;

        }

        private void webBrowser1_DocumentCompleted(object sender, EventArgs e)
        {
            SendKeys.Send("^{+}");
           

        }

        private void RelacaoTCE_Load(object sender, EventArgs e)
        {
            NovaGuia("http://transparencia.tce.ro.gov.br/transparenciatce/Servidores/Listar?tipo=inativos");
        }
    }
}
