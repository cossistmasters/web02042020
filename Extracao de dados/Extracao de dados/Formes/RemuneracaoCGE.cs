﻿using Extracao_de_dados.Banco;
using Extracao_de_dados.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Extracao_de_dados.Formes
{
    public partial class RemuneracaoCGE : Form
    {
        public RemuneracaoCGE()
        {
            InitializeComponent();
        }
        int i = 0;
        IList<string> lista_Matriculas = new List<string>();
        public void VisualizarHtml()
        {
            WebBrowser webBrowser = tabControl1.SelectedTab.Controls[0] as WebBrowser;
            string html = webBrowser.DocumentText;
        }




        private void NovaGuia(string url)
        {
            TabPage tab = new TabPage();
            tab.Text = "Portal da Transparência do Estado de Rondônia -";

            tabControl1.Controls.Add(tab);
            tabControl1.SelectTab(tabControl1.TabCount - 1);
            WebBrowser browser = new WebBrowser() { ScriptErrorsSuppressed = true };
            browser.Parent = tab;
            browser.Dock = DockStyle.Fill;
            richTextBox1.AppendText("Extraindo Dados..." + '\n');
            browser.Navigate(url);
            txtUrl.Text = url;
            txtUrl.ReadOnly = true;

            browser.DocumentCompleted += webBrowser1_DocumentCompleted;

        }

        private void webBrowser1_DocumentCompleted(object sender, EventArgs e)
        {

            ExtrairDadosRemuneracao(e);

        }


        #region Remuneraçao
        public void ExtrairDadosRemuneracao(EventArgs e)
        {
            WebBrowser browser = tabControl1.SelectedTab.Controls[0] as WebBrowser;

            if (tabControl1.SelectedTab.Text.Trim() == "Portal da Transparência do Estado de Rondônia -")
            {

                foreach (var item in BuscarMatriculas())
                {
                    lista_Matriculas.Add(item);

                }

                txtUrl.Text = "http://www.transparencia.ro.gov.br/Pessoal/DetalheServidor?ano=2020&mes=1&matricula=" + lista_Matriculas[0];

                btnIr_Click(this, e);


            }

            DetalharRemuneracao(e);
        }
        //public IList<string> BuscarValoresSolicitacoes()
        //{
        //    WebBrowser browser = tabControl1.SelectedTab.Controls[0] as WebBrowser;
        //    richTextBox1.AppendText("Dados Extraido... Numero de solicitações aprovadas" + '\n');
        //    var renderString = txtVerificador.Text.Replace('\t', Convert.ToChar(" ")).Split('\n');
        //    IList<string> solicitacao = new List<string>();
        //    for (int i = 0; i < renderString.Length; i++)
        //    {
        //        if (renderString[i].Trim() == "<tr>" && renderString[i + 1].Contains("span"))
        //        {
        //            i++;
        //            var test = renderString[i].Trim().Remove(0, 23) + " ";

        //            string capturarNumero = test.Remove(5, 12);

        //            richTextBox1.AppendText(capturarNumero + '\n');
        //            solicitacao.Add(capturarNumero);
        //        }

        //    }
        //    return solicitacao;

        //}
        public void DetalharRemuneracao(EventArgs e)
        {

            WebBrowser browser = tabControl1.SelectedTab.Controls[0] as WebBrowser;

            while (i < lista_Matriculas.Count)
            {
                var posicaoInicialDalista = lista_Matriculas[0];
                var mostrar = tabControl1.SelectedTab.Text + lista_Matriculas[i];
                if (tabControl1.SelectedTab.Text.Trim() == "Portal da Transparência do Estado de Rondônia -" + lista_Matriculas[i])
                {

                    txtVerificador.Text = "";
                    tabControl1.SelectedTab.Text = "";

                    if (browser != null)

                        txtVerificador.Text = browser.DocumentText;
                    tabControl1.SelectedTab.Text = browser.DocumentTitle + lista_Matriculas[i];

                    if (tabControl1.SelectedTab.Text.Trim() == "Portal da Transparência do Estado de Rondônia -" + lista_Matriculas[i])
                    {
                        richTextBox1.AppendText(" Extração de dados do numero de solicitação" + lista_Matriculas[i] + '\n' + '\n');
                        richTextBox1.AppendText("Redirencionando para o link http://www.transparencia.ro.gov.br/Pessoal/DetalheServidor?ano=2020&mes=1&matricula=" + lista_Matriculas[i] + '\n' + '\n');

                        VerficarRemuneracao(lista_Matriculas[i]);
                        i += lista_Matriculas.Count;



                    }


                }
                else
                {
                    browser.Navigate("http://www.transparencia.ro.gov.br/Pessoal/DetalheServidor?ano=2020&mes=1&matricula=" + lista_Matriculas[i]);
                    txtUrl.Text = "http://www.transparencia.ro.gov.br/Pessoal/DetalheServidor?ano=2020&mes=1&matricula=" + lista_Matriculas[i];
                    btnIr_Click(this, e);
                    txtVerificador.Text = browser.DocumentText;
                    tabControl1.SelectedTab.Text = browser.DocumentTitle + lista_Matriculas[i];

                }
            }
            i -= lista_Matriculas.Count - 1;
            if (i != lista_Matriculas.Count)
            {

                browser.Navigate("http://www.transparencia.ro.gov.br/Pessoal/DetalheServidor?ano=2020&mes=1&matricula=" + lista_Matriculas[i]);
                txtUrl.Text = "http://www.transparencia.ro.gov.br/Pessoal/DetalheServidor?ano=2020&mes=1&matricula=" + lista_Matriculas[i];
                btnIr_Click(this, e);
                txtVerificador.Text = browser.DocumentText;
                tabControl1.SelectedTab.Text = browser.DocumentTitle + lista_Matriculas[i];

            }
        }

        private void VerficarRemuneracao(string p)
        {
            if (txtVerificador.Text.Contains(p.ToString()))
            {
                ConexaoBanco banco = new ConexaoBanco();
                RemuneracaoIperon remuneracao = new RemuneracaoIperon();
                var objetivosGerais = txtVerificador.Text.Replace('\t', '\r').Replace("<td>", "").Replace("</td>", "").Replace('\n'.ToString(), "").Trim().Split('\r');
                var servidor = objetivosGerais[162].Trim();
                var auxilios = objetivosGerais[187].Replace("<td colspan=\"2\">R$", "").Trim();
                var previdencia = objetivosGerais[189].Replace("R$", "").Trim();
                var Matricula = objetivosGerais[161].Trim();
                var vantagens = objetivosGerais[194].Replace("R$", "").Trim();
                var impostoRenda = objetivosGerais[200].Replace("R$", "").Trim();
                var temporaririas = objetivosGerais[205].Replace("R$", "").Trim();
                var descontoDiversos = objetivosGerais[211].Replace("R$", "").Trim();
                var mesAno = objetivosGerais[164].Trim().Split('/');
                var produtividade = objetivosGerais[215].Replace("<td colspan=\"4\">R$", "").Trim();
                var redimentosTributaveis = objetivosGerais[222].Replace("<td colspan=\"2\">R$", "").Trim();
                var totalDEsconto = objetivosGerais[224].Replace("R$", "").Trim();
                var liquido = objetivosGerais[230].Replace("R$", "").Trim();
                var vencimentos = objetivosGerais[228].Replace("<td colspan=\"2\">R$", "").Trim();
                var mes = mesAno[0].Trim();
                var ano = mesAno[1].Trim();
                richTextBox1.AppendText("Matricula: " + Matricula + '\n');
                richTextBox1.AppendText("Servidor: " + servidor + '\n');
                lblContadorMatricula.Text = i.ToString();
                lblMatriculaAtual.Text = p.ToString();
                if (servidor.Trim() != "")
                {


                    remuneracao = banco.PopularDadosRemunecao(Matricula);
                    remuneracao.Ano =short.Parse(ano);
                    remuneracao.Mes = Convert.ToByte(mes);
                    remuneracao.Liquido = Convert.ToDecimal(liquido);
                    remuneracao.VbaPrevidencias = Convert.ToDecimal(previdencia);
                    remuneracao.VbaTemporarias = Convert.ToDecimal(temporaririas);
                    remuneracao.Vantagens = Convert.ToDecimal(vantagens);
                    remuneracao.VbaProdutividade = Convert.ToDecimal(produtividade);
                    remuneracao.VbaImpostorenda = Convert.ToDecimal(impostoRenda);
                    remuneracao.RendimentosTributaveis = Convert.ToDecimal(redimentosTributaveis);
                    remuneracao.Vencimentos = Convert.ToDecimal(vencimentos);
                    remuneracao.Auxilios = Convert.ToDecimal(auxilios);
                    remuneracao.TotalDescontosFinanceiros = Convert.ToDecimal(totalDEsconto);                    
                    remuneracao.VbaDescontosDiversos = Convert.ToDecimal(descontoDiversos);
                    MessageBox.Show("Servidor encontrado: " + servidor + '\n' + " No mes de:" + mes + " e ano " + ano + '\n' + "Auxilios: " + auxilios + '\n' + "previdencia: " + previdencia
                    + '\n' + "vantagem: " + vantagens + '\n' + "imposto de renda :" + impostoRenda + '\n' + "temposrias: " + temporaririas + '\n' + "desconto: " + descontoDiversos
                    + '\n' + "produtividade: " + produtividade + '\n' + "rendimentos tributaveis: " + redimentosTributaveis + '\n' + "total de desconsto: " + totalDEsconto + '\n' + "liquido: " + liquido
                     + '\n' + "vencimento: " + vencimentos);
                }
            }

        }

        #endregion

        private void btnIr_Click(object sender, EventArgs e)
        {
            WebBrowser browser = tabControl1.SelectedTab.Controls[0] as WebBrowser;
            if (browser != null)
            {
                browser.Navigate(txtUrl.Text);

            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            richTextBox1.ReadOnly = true;
            // SendKeys.Send("{DOWN}");
        }

        private void button1_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        private void RemuneracaoCGE_Load(object sender, EventArgs e)
        {
            NovaGuia("http://www.transparencia.ro.gov.br/Pessoal/DetalheServidor?ano=2020&mes=1&matricula=300006309");
        }


        public IList<string> BuscarMatriculas()
        {
            IList<string> l_Matriculas = new List<string>();
            ConexaoBanco conexao = new ConexaoBanco();
            l_Matriculas = conexao.ListarMatriculas();
            lblTotalMatricula.Text = l_Matriculas.Count.ToString().Trim();
            return l_Matriculas;

        }

        private void txtUrl_TextChanged(object sender, EventArgs e)
        {
            txtUrl.ReadOnly = true;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        ////Metodo de teste

    }
}
