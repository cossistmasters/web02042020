﻿namespace Extracao_de_dados.Formes
{
    partial class Relacao_Iperon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTotalMatricula = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblContadorMatricula = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblMatriculaAtual = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.btnIr = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.txtVerificador = new System.Windows.Forms.TextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.txtNOMEArquivo = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblTotalMatricula
            // 
            this.lblTotalMatricula.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalMatricula.AutoSize = true;
            this.lblTotalMatricula.ForeColor = System.Drawing.Color.White;
            this.lblTotalMatricula.Location = new System.Drawing.Point(885, 18);
            this.lblTotalMatricula.Name = "lblTotalMatricula";
            this.lblTotalMatricula.Size = new System.Drawing.Size(0, 13);
            this.lblTotalMatricula.TabIndex = 41;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(910, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(149, 15);
            this.label7.TabIndex = 40;
            this.label7.Text = "Total Matriculas lidas:";
            // 
            // lblContadorMatricula
            // 
            this.lblContadorMatricula.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblContadorMatricula.AutoSize = true;
            this.lblContadorMatricula.ForeColor = System.Drawing.Color.White;
            this.lblContadorMatricula.Location = new System.Drawing.Point(1093, 17);
            this.lblContadorMatricula.Name = "lblContadorMatricula";
            this.lblContadorMatricula.Size = new System.Drawing.Size(0, 13);
            this.lblContadorMatricula.TabIndex = 39;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(938, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(149, 15);
            this.label5.TabIndex = 38;
            this.label5.Text = "Total Matriculas lidas:";
            // 
            // lblMatriculaAtual
            // 
            this.lblMatriculaAtual.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMatriculaAtual.AutoSize = true;
            this.lblMatriculaAtual.ForeColor = System.Drawing.Color.White;
            this.lblMatriculaAtual.Location = new System.Drawing.Point(1244, 17);
            this.lblMatriculaAtual.Name = "lblMatriculaAtual";
            this.lblMatriculaAtual.Size = new System.Drawing.Size(0, 13);
            this.lblMatriculaAtual.TabIndex = 37;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(1160, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 15);
            this.label2.TabIndex = 36;
            this.label2.Text = "Matriculas:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(9, 11);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 35;
            this.button1.Text = "Voltar";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(95, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 15);
            this.label1.TabIndex = 33;
            this.label1.Text = "URL:";
            // 
            // txtUrl
            // 
            this.txtUrl.Location = new System.Drawing.Point(133, 12);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.Size = new System.Drawing.Size(549, 20);
            this.txtUrl.TabIndex = 32;
            // 
            // btnIr
            // 
            this.btnIr.Location = new System.Drawing.Point(530, 17);
            this.btnIr.Name = "btnIr";
            this.btnIr.Size = new System.Drawing.Size(54, 10);
            this.btnIr.TabIndex = 34;
            this.btnIr.Text = "IR";
            this.btnIr.UseVisualStyleBackColor = true;
            this.btnIr.Visible = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.tabControl1.ItemSize = new System.Drawing.Size(0, 8);
            this.tabControl1.Location = new System.Drawing.Point(36, 40);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(934, 604);
            this.tabControl1.TabIndex = 31;
            // 
            // txtVerificador
            // 
            this.txtVerificador.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.txtVerificador.Location = new System.Drawing.Point(42, 661);
            this.txtVerificador.Name = "txtVerificador";
            this.txtVerificador.Size = new System.Drawing.Size(817, 20);
            this.txtVerificador.TabIndex = 30;
            this.txtVerificador.Visible = false;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.EnableAutoDragDrop = true;
            this.richTextBox1.Location = new System.Drawing.Point(976, 40);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(329, 604);
            this.richTextBox1.TabIndex = 29;
            this.richTextBox1.Text = "";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(688, 10);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 42;
            this.button2.Text = "Importar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtNOMEArquivo
            // 
            this.txtNOMEArquivo.Location = new System.Drawing.Point(770, 13);
            this.txtNOMEArquivo.Name = "txtNOMEArquivo";
            this.txtNOMEArquivo.Size = new System.Drawing.Size(162, 20);
            this.txtNOMEArquivo.TabIndex = 43;
            // 
            // Relacao_Iperon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1315, 693);
            this.Controls.Add(this.txtNOMEArquivo);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.lblTotalMatricula);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblContadorMatricula);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblMatriculaAtual);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtUrl);
            this.Controls.Add(this.btnIr);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtVerificador);
            this.Controls.Add(this.richTextBox1);
            this.Name = "Relacao_Iperon";
            this.Text = "Relacao_Iperon";
            this.Load += new System.EventHandler(this.Relacao_Iperon_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTotalMatricula;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblContadorMatricula;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblMatriculaAtual;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtUrl;
        private System.Windows.Forms.Button btnIr;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TextBox txtVerificador;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtNOMEArquivo;
    }
}