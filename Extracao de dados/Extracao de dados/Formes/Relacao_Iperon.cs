﻿using Extracao_de_dados.Banco;
using Extracao_de_dados.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Extracao_de_dados.Formes
{
    public partial class Relacao_Iperon : Form
    {
        public Relacao_Iperon()
        {
            InitializeComponent();
        }
        int i = 0;
        IList<string> lista_Matriculas = new List<string>();
        IList<string> l_Matriculas = new List<string>();
        RemuneracaoIperon remuneracao = new RemuneracaoIperon();
        RelacaoPessoas relacao = new RelacaoPessoas();
        string situacaomodificada = "";
        string mensagem = "";
        string mensagem2 = "";
        string mes = "";
        string ano = "";
        string servidor = "";
        string auxilios = "";
        string previdencia = "";
        string Matricula = "";
        string vantagens = "";
        string impostoRenda = "";
        string temporaririas = "";
        string descontoDiversos = "";
        string produtividade = "";
        string redimentosTributaveis = "";
        string totalDEsconto = "";
        string liquido = "";
        string vencimentos = "";
        string mess = "";
        string anoo = "";
        string sublotacao = "";
        string lotacao = "";
        string situacao = "";
        string cargo = "";
        string cargaHoraria = "";
        string cpf = "";
        string cds = "";
        private void NovaGuia(string url)
        {
            TabPage tab = new TabPage();
            tab.Text = "Portal da Transparência do Estado de Rondônia -";

            tabControl1.Controls.Add(tab);
            tabControl1.SelectTab(tabControl1.TabCount - 1);
            WebBrowser browser = new WebBrowser() { ScriptErrorsSuppressed = true };
            browser.Parent = tab;
            browser.Dock = DockStyle.Fill;
            richTextBox1.AppendText("Extraindo Dados..." + '\n');
            browser.Navigate(url);
            txtUrl.Text = url;
            txtUrl.ReadOnly = true;

            browser.DocumentCompleted += webBrowser1_DocumentCompleted;

        }

        private void webBrowser1_DocumentCompleted(object sender, EventArgs e)
        {

            ExtrairDadosRemuneracao(e);


        }


        #region Remuneraçao
        public void ExtrairDadosRemuneracao(EventArgs e)
        {
            WebBrowser browser = tabControl1.SelectedTab.Controls[0] as WebBrowser;

            if (tabControl1.SelectedTab.Text.Trim() == "Portal da Transparência do Estado de Rondônia -")
            {

                foreach (var item in BuscarMatriculas())
                {
                    lista_Matriculas.Add(item.ToString().Trim());

                }

                txtUrl.Text = "http://www.transparencia.ro.gov.br/Pessoal/DetalheServidor?ano=" + ano + "&mes=" + mes + "&matricula=" + lista_Matriculas[0];

                btnIr_Click(this, e);


            }

            DetalharRemuneracao(e);
        }

        public void DetalharRemuneracao(EventArgs e)
        {

            WebBrowser browser = tabControl1.SelectedTab.Controls[0] as WebBrowser;

            while (i < lista_Matriculas.Count)
            {
                var posicaoInicialDalista = lista_Matriculas[0];
                var mostrar = tabControl1.SelectedTab.Text + lista_Matriculas[i];
                if (tabControl1.SelectedTab.Text.Trim() == "Portal da Transparência do Estado de Rondônia -" + lista_Matriculas[i])
                {

                    txtVerificador.Text = "";
                    tabControl1.SelectedTab.Text = "";

                    if (browser != null)

                        txtVerificador.Text = browser.DocumentText;
                    tabControl1.SelectedTab.Text = browser.DocumentTitle + lista_Matriculas[i];

                    if (tabControl1.SelectedTab.Text.Trim() == "Portal da Transparência do Estado de Rondônia -" + lista_Matriculas[i])
                    {
                        richTextBox1.AppendText(" Extração de dados do numero de solicitação" + lista_Matriculas[i] + '\n' + '\n');
                        richTextBox1.AppendText("Redirencionando para o linkhttp://www.transparencia.ro.gov.br/Pessoal/DetalheServidor?ano=" + ano + "&mes=" + mes + "&matricula=" + lista_Matriculas[i] + '\n' + '\n');

                        VerficarRemuneracao(lista_Matriculas[i]);
                        i += lista_Matriculas.Count;



                    }


                }
                else
                {
                    browser.Navigate("http://www.transparencia.ro.gov.br/Pessoal/DetalheServidor?ano=" + ano + "&mes=" + mes + "&matricula=" + lista_Matriculas[i]);
                    txtUrl.Text = "http://www.transparencia.ro.gov.br/Pessoal/DetalheServidor?ano=" + ano + "&mes=" + mes + "&matricula=" + lista_Matriculas[i];
                    btnIr_Click(this, e);
                    txtVerificador.Text = browser.DocumentText;
                    tabControl1.SelectedTab.Text = browser.DocumentTitle + lista_Matriculas[i];

                }
            }
            i -= lista_Matriculas.Count - 1;
            if (i != lista_Matriculas.Count)
            {

                browser.Navigate("http://www.transparencia.ro.gov.br/Pessoal/DetalheServidor?ano=" + ano + "&mes=" + mes + "&matricula=" + lista_Matriculas[i]);
                txtUrl.Text = "http://www.transparencia.ro.gov.br/Pessoal/DetalheServidor?ano=" + ano + "&mes=" + mes + "&matricula=" + lista_Matriculas[i];
                btnIr_Click(this, e);
                txtVerificador.Text = browser.DocumentText;
                tabControl1.SelectedTab.Text = browser.DocumentTitle + lista_Matriculas[i];

            }
        }

        private void VerficarRemuneracao(string p)
        {
            if (txtVerificador.Text.Contains(p.ToString()))
            {

                var objetivosGerais = txtVerificador.Text.Replace('\t', '\r').Replace("<td>", "").Replace("</td>", "").Replace('\n'.ToString(), "").Trim().Split('\r');
                var tamanhoArquivo = objetivosGerais.Length;
                if (tamanhoArquivo == 395)
                {
                    servidor = objetivosGerais[162].Trim();
                    auxilios = objetivosGerais[187].Replace("<td colspan=\"2\">R$", "").Trim();
                    previdencia = objetivosGerais[189].Replace("R$", "").Trim();
                    Matricula = objetivosGerais[161].Trim();
                    vantagens = objetivosGerais[194].Replace("R$", "").Trim();
                    impostoRenda = objetivosGerais[200].Replace("R$", "").Trim();
                    temporaririas = objetivosGerais[205].Replace("R$", "").Trim();
                    descontoDiversos = objetivosGerais[211].Replace("R$", "").Trim();
                    var mesAno = objetivosGerais[164].Trim().Split('/');
                    produtividade = objetivosGerais[215].Replace("<td colspan=\"4\">R$", "").Trim();
                    redimentosTributaveis = objetivosGerais[222].Replace("<td colspan=\"2\">R$", "").Trim();
                    totalDEsconto = objetivosGerais[224].Replace("R$", "").Trim();
                    liquido = objetivosGerais[230].Replace("R$", "").Trim();
                    vencimentos = objetivosGerais[228].Replace("<td colspan=\"2\">R$", "").Trim();
                    var situacaocargo = objetivosGerais[168].Replace("<td colspan=\"2\">", "").Trim().Split('-');
                    mess = mesAno[0].Trim();
                    anoo = mesAno[1].Trim();
                    sublotacao = objetivosGerais[176].Replace("<td colspan=\"2\">", "").Trim().ToUpper();
                    lotacao = objetivosGerais[174].Replace("<td colspan=\"2\">", "").Trim().ToUpper();
                    situacao = situacaocargo[0].Trim().ToUpper();
                    if (situacaocargo.Length > 2)
                    {
                        cargo = situacaocargo[situacaocargo.Length - 1].Trim().ToUpper();
                        relacao.Vinculo = situacaocargo[1].ToUpper().Trim();
                    }
                    else
                    {
                        cargo = situacaocargo[1].ToUpper().Trim();
                       
                    }
                    situacaomodificada =lotacao.ToUpper().Trim();
                    cargaHoraria = objetivosGerais[170].Replace("<td colspan=\"4\">", "").Trim();
                    cpf = objetivosGerais[299].Replace(" { cpf:", "").Substring(11).Trim();
                    cpf = cpf.Substring(0, 11).Replace(",", "");
                    richTextBox1.AppendText("Matricula: " + Matricula + '\n');
                    richTextBox1.AppendText("Servidor: " + servidor + '\n');
                    lblContadorMatricula.Text = i.ToString();
                    lblMatriculaAtual.Text = p.ToString();
                    ConexaoBanco banco = new ConexaoBanco();


                    if (servidor.Trim() != "")
                    {
                        relacao = banco.BuscarLotacao(lotacao);
                        relacao.Ano = Convert.ToInt32(ano);
                        relacao.CargaHoraria = Convert.ToInt32(cargaHoraria);
                        relacao.Cargo = cargo;
                        relacao.Classificacao = situacao;
                        relacao.Cpf = long.Parse(cpf.Replace(",", "").Replace("A",""));
                        relacao.LocalTrabalho = sublotacao;
                        relacao.Nome = servidor;
                        relacao.Mes = Convert.ToInt32(mes);
                        relacao.Matricula = Convert.ToInt32(Matricula);
                        remuneracao.Lotacao = lotacao;
                        remuneracao.SubLotacao = sublotacao;
                        remuneracao.Classificacao_Func = situacao;
                        remuneracao.CargaHoraria = Convert.ToInt32(cargaHoraria);
                        remuneracao.Cargo = cargo;
                        remuneracao.Nome = servidor.Trim();
                        remuneracao.Cpf = cpf.Trim();
                        remuneracao.Matricula = Matricula.Trim();
                        remuneracao.Ano = short.Parse(ano);
                        remuneracao.Mes = Convert.ToByte(mes);
                        remuneracao.Liquido = Convert.ToDecimal(liquido);
                        remuneracao.VbaPrevidencias = Convert.ToDecimal(previdencia);
                        remuneracao.VbaTemporarias = Convert.ToDecimal(temporaririas);
                        remuneracao.Vantagens = Convert.ToDecimal(vantagens);
                        remuneracao.VbaProdutividade = Convert.ToDecimal(produtividade);
                        remuneracao.VbaImpostorenda = Convert.ToDecimal(impostoRenda);
                        remuneracao.RendimentosTributaveis = Convert.ToDecimal(redimentosTributaveis);
                        remuneracao.Vencimentos = Convert.ToDecimal(vencimentos);
                        remuneracao.Auxilios = Convert.ToDecimal(auxilios);
                        remuneracao.TotalDescontosFinanceiros = Convert.ToDecimal(totalDEsconto);
                        remuneracao.VbaDescontosDiversos = Convert.ToDecimal(descontoDiversos);
                        //    MessageBox.Show("Servidor encontrado: " + servidor + '\n' + " Mes:" + mess + '\n' + " Ano: " + anoo + '\n' + "Auxilios: " + auxilios + '\n' + "previdencia: " + previdencia
                        //    + '\n' + "vantagem: " + vantagens + '\n' + "imposto de renda :" + impostoRenda + '\n' + "temposrias: " + temporaririas + '\n' + "desconto: " + descontoDiversos
                        //    + '\n' + "produtividade: " + produtividade + '\n' + "rendimentos tributaveis: " + redimentosTributaveis + '\n' + "total de desconsto: " + totalDEsconto + '\n' + "liquido: " + liquido
                        //     + '\n' + "vencimento: " + vencimentos + '\n' + "Situação: " + situacao + '\n' + "Cargo: " + cargo + '\n' + "Matricula: " + Matricula + '\n' + "Lotação: " + lotacao
                        //     + '\n' + "Carga Horaria: " + cargaHoraria + '\n' + "CPF: " + cpf + '\n' + "CDS: " +cds + '\n' + "SUBLOTACAO: " +sublotacao);
                        //}

                    }

                    Gravar(situacaomodificada);
                    richTextBox1.AppendText(mensagem + '\n' + '\n' + '\n');
                    richTextBox1.AppendText(mensagem2 + '\n' + '\n' + '\n');
                }
                else
                {
                    /// para cargos CDS onde os campos são diferentes
                    servidor = objetivosGerais[162].Trim();
                    auxilios = objetivosGerais[191].Replace("<td colspan=\"2\">R$", "").Trim();
                    previdencia = objetivosGerais[193].Replace("R$", "").Trim();
                    Matricula = objetivosGerais[161].Trim();
                    vantagens = objetivosGerais[198].Replace("R$", "").Trim();
                    impostoRenda = objetivosGerais[204].Replace("R$", "").Trim();
                    temporaririas = objetivosGerais[209].Replace("R$", "").Trim();
                    descontoDiversos = objetivosGerais[215].Replace("R$", "").Trim();
                    var mesAno = objetivosGerais[164].Trim().Split('/');
                    produtividade = objetivosGerais[219].Replace("<td colspan=\"4\">R$", "").Trim();
                    redimentosTributaveis = objetivosGerais[226].Replace("<td colspan=\"2\">R$", "").Trim();
                    totalDEsconto = objetivosGerais[228].Replace("R$", "").Trim();
                    liquido = objetivosGerais[234].Replace("R$", "").Trim();
                    vencimentos = objetivosGerais[232].Replace("<td colspan=\"2\">R$", "").Trim();
                    var situacaocargo = objetivosGerais[168].Replace("<td colspan=\"2\">", "").Split('-');
                    mess = mesAno[0].Trim();
                    anoo = mesAno[1].Trim();
                    sublotacao = objetivosGerais[176].Replace("<td colspan=\"2\">", "").Trim().ToUpper();
                    lotacao = objetivosGerais[174].Replace("<td colspan=\"2\">", "").Trim().ToUpper();
                    situacao = situacaocargo[0].Trim().ToUpper();
                    cds = objetivosGerais[180].Replace("<td colspan=\"4\">", "").Trim();
                    cargaHoraria = objetivosGerais[170].Replace("<td colspan=\"4\">", "").Trim();
                    cpf = objetivosGerais[293].Replace(" { cpf:", "").Substring(11).Trim();
                    cpf = cpf.Substring(0, 11).Replace(",", "");
                    richTextBox1.AppendText("Matricula: " + Matricula + '\n');
                    richTextBox1.AppendText("Servidor: " + servidor + '\n');
                    lblContadorMatricula.Text = i.ToString();
                    lblMatriculaAtual.Text = p.ToString();
                    ConexaoBanco banco = new ConexaoBanco();

                    if (servidor.Trim() != "")
                    {
                        relacao = banco.BuscarLotacao(lotacao);
                        if (situacaocargo.Length > 2)
                        {
                            cargo = situacaocargo[situacaocargo.Length - 1].Trim().ToUpper();
                            relacao.Vinculo = situacaocargo[1].Trim().ToUpper();


                        }
                        else
                        {
                            cargo = situacaocargo[1].ToUpper().Trim();

                        }
                        situacaomodificada = lotacao.ToUpper().Trim();
                        relacao.Ano = Convert.ToInt32(ano);
                        relacao.CargaHoraria = Convert.ToInt32(cargaHoraria);
                        relacao.Cargo = cargo;
                        relacao.Classificacao = situacao;
                        relacao.Cpf = long.Parse(cpf.Replace(",", ""));
                        relacao.LocalTrabalho = sublotacao;
                        relacao.Nome = servidor;
                        relacao.Mes = Convert.ToInt32(mes);
                        relacao.Matricula = Convert.ToInt32(Matricula);
                        remuneracao.Lotacao = lotacao;
                        remuneracao.SubLotacao = sublotacao;
                        remuneracao.Classificacao_Func = situacao;
                        remuneracao.CargaHoraria = Convert.ToInt32(cargaHoraria);
                        remuneracao.Cargo = cargo;
                        remuneracao.Nome = servidor.Trim();
                        remuneracao.Cpf = cpf.Trim();
                        remuneracao.Matricula = Matricula.Trim();
                        remuneracao.Ano = short.Parse(ano);
                        remuneracao.Mes = Convert.ToByte(mes);
                        remuneracao.CDSFG = cds.Trim();
                        remuneracao.Liquido = Convert.ToDecimal(liquido);
                        remuneracao.VbaPrevidencias = Convert.ToDecimal(previdencia);
                        remuneracao.VbaTemporarias = Convert.ToDecimal(temporaririas);
                        remuneracao.Vantagens = Convert.ToDecimal(vantagens);
                        remuneracao.VbaProdutividade = Convert.ToDecimal(produtividade);
                        remuneracao.VbaImpostorenda = Convert.ToDecimal(impostoRenda);
                        remuneracao.RendimentosTributaveis = Convert.ToDecimal(redimentosTributaveis);
                        remuneracao.Vencimentos = Convert.ToDecimal(vencimentos);
                        remuneracao.Auxilios = Convert.ToDecimal(auxilios);
                        remuneracao.TotalDescontosFinanceiros = Convert.ToDecimal(totalDEsconto);
                        remuneracao.VbaDescontosDiversos = Convert.ToDecimal(descontoDiversos);
                        //    MessageBox.Show("Servidor encontrado: " + servidor + '\n' + " Mes:" + mess + '\n' + " Ano: " + anoo + '\n' + "Auxilios: " + auxilios + '\n' + "previdencia: " + previdencia
                        //    + '\n' + "vantagem: " + vantagens + '\n' + "imposto de renda :" + impostoRenda + '\n' + "temposrias: " + temporaririas + '\n' + "desconto: " + descontoDiversos
                        //    + '\n' + "produtividade: " + produtividade + '\n' + "rendimentos tributaveis: " + redimentosTributaveis + '\n' + "total de desconsto: " + totalDEsconto + '\n' + "liquido: " + liquido
                        //     + '\n' + "vencimento: " + vencimentos + '\n' + "Situação: " + situacao + '\n' + "Cargo: " + cargo + '\n' + "Matricula: " + Matricula + '\n' + "Lotação: " + lotacao
                        //     + '\n' + "Carga Horaria: " + cargaHoraria + '\n' + "CPF: " + cpf + '\n' + "CDS: " +cds + '\n' + "SUBLOTACAO: " + sublotacao);
                        //}
                    }

                    Gravar(situacaomodificada);
                    richTextBox1.AppendText(mensagem + '\n' + '\n' + '\n');
                    richTextBox1.AppendText(mensagem2 + '\n' + '\n' + '\n');
                }

            }

        }

        #endregion

        private void btnIr_Click(object sender, EventArgs e)
        {
            WebBrowser browser = tabControl1.SelectedTab.Controls[0] as WebBrowser;
            if (browser != null)
            {
                browser.Navigate(txtUrl.Text);

            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            richTextBox1.ReadOnly = true;
            // SendKeys.Send("{DOWN}");
        }

        private void button1_Click(object sender, EventArgs e)
        {

            this.Close();
        }



        public IList<string> BuscarMatriculas()
        {

            return l_Matriculas;

        }

        private void txtUrl_TextChanged(object sender, EventArgs e)
        {
            txtUrl.ReadOnly = true;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Relacao_Iperon_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            lblContadorMatricula.Text = "";
            lblMatriculaAtual.Text = "";
            lblTotalMatricula.Text = "";
            richTextBox1.Text = "";
            lista_Matriculas = new List<string>();
            l_Matriculas = new List<string>();
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Text files | *.csv"; // file types, that will be allowed to upload
            dialog.Multiselect = false; // allow/deny user to upload more than one file at a time
            if (dialog.ShowDialog() == DialogResult.OK) // if user clicked OK
            {

                String path = dialog.FileName;
                txtNOMEArquivo.Text = dialog.SafeFileName;
                var detalhar = "";
                using (StreamReader reader = new StreamReader(new FileStream(path, FileMode.Open), new UTF8Encoding()))
                {
                    do
                    {
                        string[] sr = reader.ReadLine().Split('\"');
                        IList<string> dadosCsv = new List<string>();

                        for (int i = 0; i < sr.Length; i++)
                        {
                            dadosCsv.Add(sr[i]);
                            if (dadosCsv[i].Contains("&matricula="))
                            {
                                detalhar = dadosCsv[i];
                                var splitt = detalhar.Replace("http://www.transparencia.ro.gov.br/Pessoal/DetalheServidor?", "").Split('&');

                                mes = splitt[1].Replace("mes=", "").Trim(); ;
                                ano = splitt[0].Replace("ano=", "").Trim();
                                string martricula = splitt[2].Replace("matricula=", "").Trim();
                                l_Matriculas.Add(martricula);


                            }
                        }


                    } while (!(reader.EndOfStream));
                    NovaGuia("http://www.transparencia.ro.gov.br/Pessoal/DetalheServidor?ano=" + ano + "&mes=" + mes + "&matricula=" + l_Matriculas[0]);
                }
            }
        }


        ////Metodo de teste
        ///
        ///Refartorá esse método mais tarde não está muito adequado
        public string Gravar(string situacao)
        {
            ConexaoBanco banco = new ConexaoBanco();

            switch (situacao)
            {
                case "INATIVOS - SEPLAD":
                    mensagem = banco.InserirPensionista(relacao) == true ? "Servidor cadastrado com sucesso" : "Erro no cadastro";
                    mensagem2 = banco.InserirRemuneracao(remuneracao) == true ? "Remuneração cadastrada com sucesso" : "Erro no cadastro";
                    break;
                case "IPERON - INATIVOS":
                    mensagem = banco.InserirPensionista(relacao) == true ? "Servidor cadastrado com sucesso" : "Erro no cadastro";
                    mensagem2 = banco.InserirRemuneracao(remuneracao) == true ? "Remuneração cadastrada com sucesso" : "Erro no cadastro";
                    break;
                case "IPERON - ASSEMBLEIA LEGISLATIVA INATIVOS":
                    mensagem = banco.InserirPensionista(relacao) == true ? "Servidor cadastrado com sucesso" : "Erro no cadastro";
                    mensagem2 = banco.InserirRemuneracao(remuneracao) == true ? "Remuneração cadastrada com sucesso" : "Erro no cadastro";
                    break;
                case "IPERON - INATIVOS DEFENSORES":
                    mensagem = banco.InserirPensionista(relacao) == true ? "Servidor cadastrado com sucesso" : "Erro no cadastro";
                    mensagem2 = banco.InserirRemuneracao(remuneracao) == true ? "Remuneração cadastrada com sucesso" : "Erro no cadastro";
                    break;
                case "IPERON - COMISSIONADOS":
                    relacao.Vinculo = "SEM VINCULO";
                    mensagem = banco.InserirComissionadosEfetivos(relacao) == true ? "Servidor cadastrado com sucesso" : "Erro no cadastro";
                    mensagem2 = banco.InserirRemuneracao(remuneracao) == true ? "Remuneração cadastrada com sucesso" : "Erro no cadastro";
                    break;
                case "IPERON - INST DE PREV DO EST DE RO  00":
                    relacao.Vinculo = relacao.Vinculo == null ? "EFETIVO" : relacao.Vinculo;
                    mensagem = banco.InserirComissionadosEfetivos(relacao) == true ? "Servidor cadastrado com sucesso" : "Erro no cadastro";
                    mensagem2 = banco.InserirRemuneracao(remuneracao) == true ? "Remuneração cadastrada com sucesso" : "Erro no cadastro";
                    break;
              

                default:
                    mensagem = "Servidor não cadastrado";
                    break;

            }
            return mensagem;
        }

    }





}
