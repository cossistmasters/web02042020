﻿using Extracao_de_dados.Banco;
using Extracao_de_dados.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

namespace Extracao_de_dados.Formes
{
    public partial class ViagensEDiarias : Form
    {
        ViagensE_Diarias vd = new ViagensE_Diarias();
        ConexaoBanco banco = new ConexaoBanco();
        public ViagensEDiarias()
        {
            
            InitializeComponent();
            
            
        }
        IList<string> solicitacao = new List<string>();
        public void VisualizarHtml()
        {
            WebBrowser webBrowser = tabControl1.SelectedTab.Controls[0] as WebBrowser;
            string html = webBrowser.DocumentText;
        }

        private void ViagensEDiarias_Load(object sender, EventArgs e)
        {

            NovaGuia("http://frota.sistemas.ro.gov.br/?data_ida=&data_retorno=&placa=&unidade=70&fonte=&q=filtrar");
           
        }


        private void NovaGuia(string url)
        {
            TabPage tab = new TabPage();
            tab.Text = "Nova Guia";
           
            tabControl1.Controls.Add(tab);
            tabControl1.SelectTab(tabControl1.TabCount - 1);
            WebBrowser browser = new WebBrowser() { ScriptErrorsSuppressed = true };
            browser.Parent = tab;
            browser.Dock = DockStyle.Fill;           
            richTextBox1.AppendText("Extraindo Dados..." + '\n');
            browser.Navigate(url);
            txtUrl.Text = url;
            
            browser.DocumentCompleted += webBrowser1_DocumentCompleted;
           
        }

        private void webBrowser1_DocumentCompleted(object sender, EventArgs e)
        {
            SendKeys.Send("^{+}");
            ExtrairDadosDiarias(e);

        }


        #region Frotas
        public void ExtrairDadosDiarias(EventArgs e)
        {
            WebBrowser browser = tabControl1.SelectedTab.Controls[0] as WebBrowser;

            if (browser != null)
                txtVerificador.Text = browser.DocumentText;
            tabControl1.SelectedTab.Text = browser.DocumentTitle;

            if (tabControl1.SelectedTab.Text == "Sistema Frota - Transparência Pública")
            {

                foreach (var item in BuscarValoresSolicitacoes())
                {
                    solicitacao.Add(item);

                }

                txtUrl.Text = "http://frota.sistemas.ro.gov.br/SolicitacoesViagem/transparencia_imprimir/" + solicitacao[0];

                btnIr_Click(this, e);

            }

            DetalharAutorizacao(e);
        }
        public IList<string> BuscarValoresSolicitacoes()
        {
           
            WebBrowser browser = tabControl1.SelectedTab.Controls[0] as WebBrowser;
            richTextBox1.AppendText("Dados Extraido... Numero de solicitações aprovadas" + '\n');
            var renderString = txtVerificador.Text.Replace('\t', Convert.ToChar(" ")).Split('\n');
            IList<string> solicitacao = new List<string>();
            for (int i = 0; i < renderString.Length; i++)
            {
                if (renderString[i].Trim() == "<tr>" && renderString[i + 1].Contains("span"))
                {
                    i++;
                    var test = renderString[i].Trim().Remove(0, 23) + " ";

                    string capturarNumero = test.Remove(5, 12);

                    richTextBox1.AppendText(capturarNumero + '\n');
                    solicitacao.Add(capturarNumero);
                }

            }
            return solicitacao;

        }
        public void DetalharAutorizacao(EventArgs e)
        {

            WebBrowser browser = tabControl1.SelectedTab.Controls[0] as WebBrowser;
            for (int i = 0; i < solicitacao.Count; i++)
            {
                if (tabControl1.SelectedTab.Text.Trim() == "Solicitação e Autorização de Viagem e Diárias Nº " + solicitacao[i].Trim())
                {
                    txtVerificador.Text = "";
                    tabControl1.SelectedTab.Text = "";
                    richTextBox1.AppendText(" Extração de dados do numero de solicitação" + solicitacao[i] + '\n' + '\n');
                    richTextBox1.AppendText("Redirencionando para o link http://frota.sistemas.ro.gov.br/SolicitacoesViagem/transparencia_imprimir/" + solicitacao[i] + '\n' + '\n');

                    if (browser != null)

                        txtVerificador.Text = browser.DocumentText;
                    tabControl1.SelectedTab.Text = browser.DocumentTitle;

                    if (tabControl1.SelectedTab.Text == "Solicitação e Autorização de Viagem e Diárias Nº " + solicitacao[i].Trim())
                    {
                        var dadosAutorizados = txtVerificador.Text.Replace('\t', Convert.ToChar(" ")).Split('\n');
                        var autorizacao = dadosAutorizados[56].Trim().Replace(".", "");
                        richTextBox1.AppendText("Número de autorização: " + autorizacao + '\n' + '\n' + '\n' + '\n');
                        vd.NumroSolicitacao =solicitacao[i].Trim();
                        vd.NumeroAutorizacao = autorizacao.Trim();
                       Verficar2();
                        var cont = 0;
                        foreach (var item in vd.QuantidadeDiarias)
                        {
                           
                            vd.ListaValorTotalPorPessoa.Add(Convert.ToDecimal(item.Replace('.',','))*Convert.ToDecimal(vd.ValorUnitaioDiarias[cont].Replace('.',',')));
                            cont++;
                        }
                        banco.ListarPessoas(vd);
                        banco.InserirDiariarias(vd);
                        MessageBox.Show("Numero de solicitação" + solicitacao[i] + "salvo com sucesso deseja continuar?");
                        i += solicitacao.Count;
                        vd = new ViagensE_Diarias();
                    }

                }
                else
                {
                    browser.Navigate("http://frota.sistemas.ro.gov.br/SolicitacoesViagem/transparencia_imprimir/" + solicitacao[i]);
                    txtUrl.Text = "http://frota.sistemas.ro.gov.br/SolicitacoesViagem/transparencia_imprimir/" + solicitacao[i];
                    btnIr_Click(this, e);
                    txtVerificador.Text = browser.DocumentText;
                    tabControl1.SelectedTab.Text = browser.DocumentTitle;
                }

            }
        }
        private void Verficar2()
        {
            var objetivosGerais = txtVerificador.Text.Replace('\t', '\r').Split('\r');
            IList<string> valor1 = new List<string>();
            IList<string> listaCPf = new List<string>();
            string objetivoViagem = "";
            var idaViagem = "";
            var diarias = "";
            var valorDiarias = "";
            var totalpordiarias = "";
            decimal subtotal = 0;
            var voltaViagem = "";
            string[] tipoviagem;
            string valor67 = "";
            var modoviagem = "";
            int cont = 0;
            for (int i = 0; i < objetivosGerais.Length; i++)
            {
                valor1.Add(objetivosGerais[i].Trim());
                valor1.Remove("");

            }


            for (int o = 0; o < valor1.Count; o++)
            {
                if (valor1[o].Contains("<!--<"))
                {
                    var varredura = valor1[o].Replace("<!--<strong>", "");
                    cont = o;
                    while (!valor1[cont].Contains(">-->"))
                    {

                        objetivoViagem = objetivoViagem + valor1[cont].Replace("<!--<strong>", "");
                        cont++;
                    }
                    if (valor1[o].Contains("<!--<strong>") && valor1[o].Contains("</strong>-->"))
                    {
                        objetivoViagem = objetivoViagem + valor1[cont].Replace("<!--<strong>", "").Replace("</strong>-->", "");
                    }
                   
                }
                if (valor1[o].Contains("<strong>X</strong>"))
                {
                    tipoviagem = valor1[o].TrimEnd().Replace("&nbsp", "").Split(';');
                    for (int i = 0; i < tipoviagem.Length; i++)
                    {
                        if (tipoviagem[i] != "")
                        {
                            if (tipoviagem[i].Trim().Equals("( <strong>X</strong> )TERRESTRE"))
                            {
                                modoviagem = tipoviagem[i].Replace("( <strong>X</strong> )", "").Trim();
                                richTextBox1.AppendText("Tipo da Viagem: " + modoviagem + '\n' + '\n');
                                vd.TipoViagem = modoviagem.Trim();
                            }
                            if (tipoviagem[i].Trim().Equals("( <strong>X</strong> )AÉREO"))
                            {
                                modoviagem = tipoviagem[i].Replace("( <strong>X</strong> )", "").Trim();
                                richTextBox1.AppendText("Tipo da Viagem: " + modoviagem + '\n' + '\n');
                                vd.TipoViagem = modoviagem.Trim();
                            }
                            if (tipoviagem[i].Trim().Equals("( <strong>X</strong> )OUTRO"))
                            {
                                modoviagem = tipoviagem[i].Replace("( <strong>X</strong> )", "").Trim();
                                richTextBox1.AppendText("Tipo da Viagem: " + modoviagem + '\n' + '\n');
                                vd.TipoViagem = modoviagem.Trim();
                            }

                        }

                    }
                }
                if (valor1[o].Contains(".") && valor1[o].Contains("-") && valor1[o].Contains("<td>"))
                {
                    var verificarCPF = valor1[o].Replace(".", "").Replace("-", "").Replace("<td>", "").Replace("</td>", "").Trim();
                    if (verificarCPF.Length == 11)
                    {
                        listaCPf.Add(valor1[o].Replace(".", "").Replace("-", ""));
                        richTextBox1.AppendText(" CPF  " + '\n' + verificarCPF + '\n' + '\n');
                        vd.ListaCpf.Add(verificarCPF.Trim());
                    }


                }
                valorDiarias = valor1[o].Trim().Contains(".") || valor1[o].Trim().Contains(",") ? valor1[o].Trim() : "";
                valorDiarias = valorDiarias.Contains("<td>") ? valorDiarias.Replace("<td>", "").Replace("</td>", "") : valorDiarias;
                totalpordiarias = valor1[o].Contains("R$") && !valor1[o].Contains("<!--") ? valor1[o].Trim().Replace("<td>R$", "").Replace("R$", "").Replace("</td>", "") : "";
                
                diarias = valor1[o].Contains("<td>") ? valor1[o].Replace("<td>", "").Replace("</td>", "") : "";
                idaViagem = valor1[o].Contains("Ida") ? valor1[o].Replace("Ida <strong>", "").Replace("</strong>", "") : "";
                voltaViagem = valor1[o].Contains("Retorno <strong>") ? valor1[o].Replace("Retorno <strong>", "").Replace("</strong>", "") : "";
                if (totalpordiarias != "")
                {
                    subtotal= Convert.ToDecimal(totalpordiarias);
                    richTextBox1.AppendText(" Valor total da Diaria " + totalpordiarias + '\n' + '\n');
                    decimal total=0;
                    total+=Convert.ToDecimal(totalpordiarias);
                    vd.ValorTotalDaViagem = subtotal >= total ? total :0;


                }
                if (valorDiarias != "" && valorDiarias.Length == 6)
                {
                    valor67 = valor1[o].Trim().Contains(".") || valor1[o].Trim().Contains(",") ? valor1[o] : "";
                    if (valorDiarias.Trim().Contains(".") || valorDiarias.Trim().Contains(","))
                    {
                        richTextBox1.AppendText(" Valor da Diaria " + valorDiarias + '\n' + '\n');
                        vd.ValorUnitaioDiarias.Add(valorDiarias.Trim());

                    }
                    else
                    {
                        richTextBox1.AppendText(" Valor da Diaria " + valor67 + '\n' + '\n');
                        vd.ValorUnitaioDiarias.Add(valor67.Trim());
                    }
                    
                }
                if (diarias != "" && diarias.Trim().Length == 4 || diarias.Trim().Length == 5)
                {
                    if (diarias.Trim().Contains(".") || diarias.Trim().Contains("."))
                    {
                        
                        richTextBox1.AppendText(" Quantidades de Diarias " + diarias + '\n' + '\n');
                        vd.QuantidadeDiarias.Add(diarias);
                    }
                    
                }

                if (idaViagem != "")
                {
                    richTextBox1.AppendText(" Ida da Viagem " + idaViagem + '\n' + '\n');
                    vd.DataIda = idaViagem;
                }
                if (voltaViagem != "")
                {
                    richTextBox1.AppendText(" Retorno da Viagem " + voltaViagem + '\n' + '\n');
                    vd.DataVolta = voltaViagem.Trim();
                }

                

            }
            richTextBox1.AppendText(" Objetivo da Viagem " + '\n' + objetivoViagem + '\n' + '\n');
            vd.ObjetivoDaViagem = objetivoViagem.Trim();
            
            valor1.Clear();


        }
        #endregion

        private void btnIr_Click(object sender, EventArgs e)
        {
            WebBrowser browser = tabControl1.SelectedTab.Controls[0] as WebBrowser;
            if (browser != null)
            {
                browser.Navigate(txtUrl.Text);

            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            richTextBox1.ReadOnly = true;
            SendKeys.Send("{DOWN}");
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            this.Close();
        }
    }
}
